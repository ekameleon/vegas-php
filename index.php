<?php

// ------------------------ imports

use system\signals\Signal;
use system\signals\Receiver;
use system\logging\Logger;

// ---------------------------------- constants

define ( '__APP__'  , __DIR__ . "/" ) ;

// ---------------------------------- errors

error_reporting( E_ALL & ~E_DEPRECATED );

// ---------------------------------- config

ini_set( 'display_errors'         , 'On' ) ;
ini_set( 'display_startup_errors' , 'On' ) ;

// ---------------------------------- autoloader

require __APP__ . 'vendor/autoload.php';

// ---------------------------------- logger

$logger = new Logger( __APP__ . 'logs/' , Logger::DEBUG ) ;

set_error_handler     ( [ $logger , 'onError'     ] ) ;
set_exception_handler ( [ $logger , 'onException' ] ) ;

// ----------------------------------

class Slot implements Receiver
{
    public function __construct( Logger $logger )
    {
        $this->logger = $logger ;
    }

    public $logger ;

    public function receive()
    {
        $this->logger->debug('hello receiver') ;
    }
}

// ----------------------------------

$slot1 = function() use ( $logger )
{
    $logger->debug('hello slot') ;
};

$slot2 = new Slot( $logger ) ;

// ----------------------------------

$signal = new Signal() ;

$signal->connect( $slot1 ) ;
$signal->connect( $slot2 ) ;

$signal->emit() ;


?>
