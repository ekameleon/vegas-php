<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace graphics\geom;

use core\Maths ;

/**
 * The <code class="prettyprint">AspectRatio</code> class encapsulates the width and height
 * of an object and indicates this aspect ratio.
 * <p>The aspect ratio of an image is the ratio of its width to its height.
 * For example, a standard NTSC television set uses an aspect ratio of 4:3,
 * and an HDTV set uses an aspect ratio of 16:9. A computer monitor with
 * a resolution of 640 by 480 pixels also has an aspect ratio of 4:3.
 * A square has an aspect ratio of 1:1.</p>
 * <p><b>Note :</b>This class use integers to specified the aspect ratio.</p>
 */
class AspectRatio
{
    /**
     * Creates a new AspectRatio instance.
     * @param width The width int value use to defines the aspect ratio value.
     * @param height The height int value use to defines the aspect ratio value.
     * @param lock This boolean flag indicates if the aspect ratio must be keeped when the width or height values changes.
     */
    public function __construct(  $width = 0 , $height = 0 , $lock = false )
    {
        $this->_width  = $width  ;
        $this->_height = $height ;
        $this->_GCD() ;
        $this->_locked = $lock   ;
    }

    /**
     * The verbose mode.
     */
    public $verbose = FALSE ;

    /**
     * Determinates the greatest common divisor if the current object.
     * <p>This property cast the width and the height Number in two int objects to calculate the value.</p>
     * <p>The floating values are ignored and convert in integers.</p>
     */
    public function getGCD()
    {
        return $this->_gcd ;
    }

    /**
     * Returns the height value.
     */
    public function getHeight()
    {
        return $this->_height ;
    }

    /**
     * Returns the width value.
     */
    public function getWidth()
    {
        return $this->_width ;
    }

    /**
     * Returns true if the object is locked.
     * @return true if the object is locked.
     */
    public function isLocked()
    {
        return $this->_locked ;
    }

    /**
     * Locks the object.
     */
    public function lock()
    {
        $this->_locked = TRUE ;
    }

    /**
     * Set the height value.
     */
    public function setHeight( $value )
    {
        $this->_height = intval($value) ;
        if ( $this->_locked )
        {
            $this->_w = intval($this->_height * $this->_aspW / $this->_aspH ) ;
        }
        else
        {
            $this->_GCD() ;
        }
    }

    /**
     * Set the width value.
     */
    public function setWidth( $value )
    {
        $this->_width = intval($value) ;
        if ( $this->_locked )
        {
            $this->_h = intval($this->_width * $this->_aspH / $this->_aspW ) ;
        }
        else
        {
            $this->_GCD() ;
        }
    }

    /**
     * Unlocks the object.
     */
    public function unlock()
    {
        $this->_locked = FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return a String representation of the object.
     */
    public function __toString()
    {
        $exp = $this->_aspW . ":" . $this->_aspH ;
        return "[" . get_class( $this ) . ']' ;
        if ( $this->verbose )
        {
            return "[" + get_class( $this ) + " width:" + $this->getWidth() + " height:" + $this->getHeight() + " ratio:{" + $s + "}]" ;
        }
        else
        {
            return $s ;
        }
    }

    /**
     * @private
     */
    private $_aspW ;

    /**
     * @private
     */
    private $_aspH ;

    /**
     * @private
     */
    private $_gcd ;

    /**
     * The width value.
     * @private
     */
    protected $_height ;

    /**
     * @private
     */
    private $_locked ;

    /**
     * The width value.
     * @private
     */
    protected $_width ;

    /**
     * @private
     */
    private function _GCD()
    {
        $this->_gcd  = Maths::gcd( intval($this->_width) , intval($this->_height) ) ;
        $this->_aspW = intval(intval($this->_width)  / $this->_gcd) ;
        $this->_aspH = intval(intval($this->_height) / $this->_gcd) ;
    }
}

?>
