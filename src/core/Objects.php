<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace core;

/**
 * The string tool class.
 */
class Objects
{
    /**
     * Compress the passed in indexed object and remove all the empty properties.
     * <p>Usage :</p>
     * <pre>
     * use core\Objects ;
     *
     * $object = new stdClass();
     *
     * $object->id          = 1;
     * $object->created     = NULL;
     * $object->name        = "hello world";
     * $object->description = NULL;
     *
     * echo json_encode( Objects::compress( $object , ["description"] ) ) ;
     * @param obj The object to compress
     * @param array except The list of properties to exclude.
     */
    public static function compress( $obj , $excludes = NULL )
    {
        if( $obj )
        {
            foreach( $obj as $key => $value )
            {
                if( is_array( $excludes ) && in_array( $key , $excludes ) )
                {
                    continue ;
                }

                if( !isset( $obj[$key] ) )
                {
                    unset( $obj[$key] );
                }
            }
        }
        return $obj;
    }

    /**
     * Compress the passed in object and remove all the empty properties.
     * <p>Usage :</p>
     * <pre>
     * use core\Objects ;
     *
     * $object = new stdClass();
     *
     * $object->id          = 1;
     * $object->created     = NULL;
     * $object->name        = "hello world";
     * $object->description = NULL;
     *
     * echo json_encode( Objects::compress( $object , ["description"] ) ) ;
     * @param obj The object to compress
     * @param array except The list of properties to exclude.
     */
    public static function compressObject( $obj , $excludes = NULL )
    {
        $properties = get_object_vars( $obj );
        foreach( $properties as $key => $value )
        {
            if( is_array($excludes) && in_array( $key , $excludes ) )
            {
                continue ;
            }

            if( isset( $obj->{ $key } ) )
            {
                unset( $obj->{ $key } );
            }
        }
        return $obj;
    }
}

?>