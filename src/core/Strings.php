<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace core;

/**
 * The string tool class.
 */
class Strings
{
    /**
     * Returns TRUE if the specified source expression contains the passed-in word.
     * @return TRUE if the specified source expression contains the passed-in word.
     */
    public static function contains( $source , $needle )
    {
        if( !isset($source) || empty($source) )
        {
            return FALSE ;
        }
        return strpos( $source , $needle ) !== false ;
    }

    /**
     * Determines wether the end of a string matches the specified value.
     * <p><b>Example :</b></p>
     * <pre>
     * use core\Strings ;
     * 
     * $file = "hello.txt" ;
     * 
     * echo Strings::endsWith( $file , "txt" ) ? "OK" : "NO" ;
     * </pre>
     * 
     * @param string source the string reference.
     * @param string value the value to find in first in the source.
     * 
     * @return true if the value is find in first.
     */
    public static function endsWith( $source , $value )
    {
        return $value === "" || substr($source, -strlen($value)) === $value;
    }

    /**
     * Quick and fast format of a string using indexed parameters only.
     * <p>Usage :</p>
     * <ul>
     * <li><code>fastformat( $pattern, ...args ):String</code></li>
     * <li><code>fastformat( $pattern, [arg0,arg1,arg2,...] )</code></li>
     * </ul>
     * <pre>
     * use core\Strings ;
     * 
     * echo Strings::fastformat( "hello {0}", "world" ) );
     * //output: "hello world"
     * 
     * echo Strings::fastformat( "hello {0} {1} {2}", [ "the", "big", "world" ] ) );
     * //output: "hello the big world"
     * </pre>
     * @return string The formatted string expression.
     */
    public static function fastformat() 
    { 
        $args    = func_get_args() ;
        $pattern = array_shift($args) ;

        if( empty( $pattern ) || ( $pattern == "" ) )
        {
            return "" ;
        }
        
        $len = count( $args ) ;

        if( ( $len == 1 ) && is_array( $args[0] ) )
        {
            $args = $args[0] ;
            $len  = count( $args ) ;
        }

        if( $len > 0 )
        {
            for( $i = 0 ; $i < $len ; $i++ )
            {
                $pattern = preg_replace( '/\\{' . $i . '\\}/' , $args[ $i ] , $pattern ) ;
            }
        }

        return $pattern ;
    } 

    /**
     * Formart the request arguments.
     */
    public static function formatRequestArgs( $params, $useNow = FALSE )
    {
        $str  = "" ;

        if( count($params) > 0 )
        {
            $first = TRUE;

            foreach( $params as $key => $value )
            {
                if( ($key == "from") && $useNow ) 
                { 
                    $value = "now"; 
                }

                if( $first )
                {
                    $str .= '?' ;
                    $first = FALSE;
                }
                else
                {
                    $str .= '&' ;
                }
                $str .= $key . '=' . $value ;
            }
        }

        return $str;
    }

   /**
    * Clean an html expression and returns a plain text.
    */
    public static function sanitize( $content, $encoding = 'UTF-8' )
    {
        $content = str_replace("<strong>","<b>",$content);
        $content = str_replace("</strong>","</b>",$content);
        $content = str_replace("<h1>"," ",$content);
        $content = str_replace("</h1>"," ",$content);
        $content = str_replace("<h2>"," ",$content);
        $content = str_replace("</h2>"," ",$content);
        $content = str_replace("<h3>"," ",$content);
        $content = str_replace("</h3>"," ",$content);
        $content = str_replace("<li>"," ",$content);
        $content = str_replace("</li>"," ",$content);
        $content = str_replace("// <![CDATA["," ",$content);
        $content = str_replace("// ]]>"," ",$content);
        $content = str_replace("<span[^>]*>"," ",$content);
        $content = str_replace("</span>"," ",$content);
        $content = str_replace("<div[^>]*>"," ",$content);
        $content = str_replace("</div>"," ",$content);
        $content = str_replace("<!--more-->"," ",$content);
        $content = str_replace("’" , "' ", $content);
        $content = str_replace("…" , "...", $content);
        $content = str_replace("“" , "\"", $content);
        $content = str_replace("œ" , "oe", $content);
        $content = nl2br($content);
        $content = str_replace("<br />"," ",$content);
        $content = preg_replace("/(.)?\\n/"," ",$content);
        $content = htmlentities($content, 0, $encoding, false);
        $content = preg_replace("{<img[^>]*/>}", "", $content);
        //$content = preg_replace("{<a[^>]*>[[:space:]|&nbsp;]*</a>}", "", $content);

        return $content ;      
    }

    /**
     * Checks if this string starts with the specified prefix.
     * <p><b>Example :</b></p>
     * <pre>
     * use core\Strings ;
     * 
     * $file = "hello.txt" ;
     * 
     * echo Strings::startsWith( $file , "hello" ) ? "OK" : "NO" ;
     * </pre>
     * 
     * @param string source the string reference.
     * @param string value the value to find in first in the source.
     * 
     * @return true if the value is find in first.
     */
    public static function startsWith( $source , $value )
    {
        return $value === "" || strpos($source, $value) === 0 ;
    }

    /**
     * Encodes the specified uri according to RFC 3986.
     */
    public static function urlencode( $uri ) 
    {
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D' , '%27');
        $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]" , "'");
        return str_replace( $entities , $replacements , urlencode($uri) ) ;
    }
}

?>