<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace core;

/**
 * The Maths tool class.
 */
class Maths
{
    /**
     * This constant defines the radius of the earth in meter ( 6371 km ).
     */
    const EARTH_RADIUS_IN_METERS  = 6371000 ;

    /**
     * Bounds a numeric value between 2 numbers.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     *
     * echo \core\Maths::clamp(4, 5, 10) ; // 5
     * echo \core\Maths::clamp(12, 5, 10) ; // 10
     * echo \core\Maths::clamp(6, 5, 10) ; // 5
     * echo \core\Maths::clamp(NAN, 5, 10) ; // NAN
     * </pre>
     * @param value the value to clamp.
     * @param min the min value of the range.
     * @param max the max value of the range.
     * @return a bound numeric value between 2 numbers.
     */
    public static function clamp( $value, $min, $max )
    {
        if (is_nan( $value ))
        {
            return NAN ;
        }
        if (is_nan( $min ))
        {
            $min = $value ;
        }
        if (is_nan( $max ))
        {
            $max = $value ;
        }
        return max( min( $value, $max ) , $min ) ;
    }

    /**
     * Returns the greatest common divisor with the Euclidean algorithm.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     * use core\Maths ;
     *
     * echo("Maths::gcd(320,240) : " . Maths::gcd(320,240) ) ; // gcd(320,240) : 80
     * </pre>
     * @param i1 The first integer value.
     * @param i2 The second integer value.
     * @return the greatest common divisor with the Euclidean algorithm.
     */
    public static function gcd( $i1 , $i2 )
    {
        if ( $i2 == 0 )
        {
            return $i1 ;
        }
        else if ( $i1 == $i2 )
        {
            return $i1 ;
        }
        else
        {
            $t ;
            while( $i2 != 0 )
            {
                $t  = $i2 ;
                $i2 = $i1 % $i2 ;
                $i1 = $t ;
            }
            return $i1 ;
        }
    }

    /**
     * The haversine formula is an equation important in navigation, giving great-circle distances between two points on a sphere from
     * their longitudes and latitudes.This algorithm is way faster than the Vincenty Formula but less accurate.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     * use core\Maths ;
     *
     * $position1 = [ "x" => 37.422045 , "y" => -122.084347 ] ; // Google HQ
     * $position2 = [ "x" => 37.77493  , "y" => -122.419416 ] ; // San Francisco, CA
     *
     * trace( haversine( $position1['x'] , $position1['y'] , $position2['x'] , $position2['y'] ) ) ; // 49 103.007 meters
     * </pre>
     * @param latitude1 The first latitude coordinate.
     * @param longitude1 The first longitude coordinate.
     * @param latitude2 The second latitude coordinate.
     * @param longitude2 The second longitude coordinate.
     * @param radius The optional radius of the sphere (by default the function use the earth's radius, mean radius = 6,371km) .
     * @return The distance between two points on a sphere from their longitudes and latitudes.
     * @see core\Maths::EARTH_RADIUS_IN_METERS
     */
    public static function haversine( $latitude1, $longitude1, $latitude2, $longitude2 , $radius = 6371000 )
    {
        $dLat = deg2rad( $latitude2  - $latitude1  );
        $dLng = deg2rad( $longitude2 - $longitude1 );

        $a = sin( $dLat * .5 ) * sin( $dLat * .5 ) + cos( deg2rad($latitude1) ) * cos( deg2rad($latitude2) ) * sin( $dLng * .5 ) * sin( $dLng * .5 );
        $c = round( 2 * atan2( sqrt($a) , sqrt(1-$a) ) * $radius , 3 ) ;

        return ( $c == $c ) ? $c : 0 ;
    }
}

?>
