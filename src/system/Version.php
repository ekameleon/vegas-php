<?php

namespace system;

/**
 * Example :
 * <?php
 * use system\Version ;
 *
 * $version1 = new Version( 2 , 1 , 1 , 110 ) ;
 * $version2 = new Version( 3 , 1 , 1 , 110 ) ;
 * $version3 = new Version( 1 , 2 , 3 , 4 ) ;
 *
 * $version1->major = 3 ;
 *
 * $logger->debug('version  : ' . $version1 ) ;
 * $logger->info( 'major    : ' . $version1->major ) ;
 * $logger->info( 'minor    : ' . $version1->minor ) ;
 * $logger->info( 'build    : ' . $version1->build ) ;
 * $logger->info( 'revision : ' . $version1->revision ) ;
 *
 * $logger->info( 'version 1 : ' . $version1->valueOf() ) ;
 * $logger->info( 'version 2 : ' . $version2->valueOf() ) ;
 * $logger->info( 'version 3 : ' . $version3->valueOf() ) ;
 *
 * $logger->info( "equals('toto')    : " . ($version1->equals('toto')    ? 'true' : 'false' )) ;
 * $logger->info( "equals($version2) : " . ($version1->equals($version2) ? 'true' : 'false' )) ;
 * $logger->info( "equals($version3) : " . ($version1->equals($version3) ? 'true' : 'false' )) ;
 */
class Version implements Equatable
{
    /**
     * Creates a new Version instance.
     */
    public function __construct( $major = 0, $minor = 0, $build = 0, $revision = 0 )
    {
        $this->_value = ($major << 28) | ($minor << 24) | ($build << 16) | $revision ;
    }

    /**
     * The fields limit.
     */
    public $fields = 0 ;

    /**
     * The separator expression.
     */
    public $separator = '.' ;

    /**
     * We don't really need an equals method as we override the valueOf, we can do something as
     * <pre class="prettyprint">
     * var v1:Version = new Version( 1,0,0,0 );
     * var v2:Version = new Version( 1,0,0,0 );
     * trace( int(v1) == int(v2) ); //true
     * </pre>
     * A cast to Number/int force the valueOf, not ideal but sufficient, and the same for any other operators.
     * But as we keep IEquatable for now, then we have no reason to not use it.
     */
    public function equals( $object )
    {
        if( $object instanceof Version )
        {
            return (bool) ($this->_value == $object->valueOf()) ;
        }

        return FALSE ;
    }

    /**
     * Indicates the build value of this version.
     */
    public function getBuild()
    {
        return $this->RRR(($this->_value & 0x00FF0000) , 16 );
    }

    /**
     * Indicates the major value of this version.
     */
    public function getMajor()
    {
        return $this->RRR($this->_value , 28 );
    }

    /**
     * Indicates the minor value of this version.
     */
    public function getMinor()
    {
        return $this->RRR( ($this->_value & 0x0F000000) , 24 ) ;
    }

    /**
     * Indicates the revision value of this version.
     */
    public function getRevision()
    {
        return $this->_value & 0x0000FFFF;
    }

    /**
     * Set the build value.
     */
    public function setBuild( $value )
    {
        $this->_value = ($this->_value & 0xFF00FFFF) | ($value << 16);
    }

    /**
     * Set the major value.
     */
    public function setMajor( $value )
    {
        $this->_value = ($this->_value & 0x0FFFFFFF) | ($value << 28);
    }

    /**
     * Set the minor value.
     */
    public function setMinor( $value )
    {
        $this->_value = ($this->_value & 0xF0FFFFFF) | ($value << 24);
    }

    /**
     * Set the revision value.
     */
    public function setRevision( $value )
    {
        $this->_value = ($this->_value & 0xFFFF0000) | $value;
    }

    /**
     * Returns a string representation of the object.
     * @return a string representation of the object.
     */
    public function __toString()
    {
        $data =
        [
            $this->major,
            $this->minor,
            $this->build ,
            $this->revision
        ];

        if( ( $this->fields > 0) && ( $this->fields < 5 ) )
        {
            $data = array_slice( $data , 0 , $fields ) ;
        }
        else
        {
            $i = 0 ;
            $l = count($data);
            for( $i = $l-1 ; $i>0 ; $i-- )
            {
                if( $data[$i] == 0 )
                {
                    array_pop( $data );
                }
                else
                {
                    break;
                }
            }
        }

        return implode( $this->separator , $data ) ;
    }

    /**
     * Returns the primitive value of the object.
     * @return the primitive value of the object.
     */
    public function valueOf()
    {
        return $this->_value ;
    }

    // ------- MAGICK

    /**
     * @private
     */
    public function __get( $name )
    {
        $method = NULL ;
        if( $name != '' )
        {
            $method = 'get' . ucfirst($name) ;
        }

        if( method_exists( $this, $method ) )
        {
            return $this->{ $method }();
        }
        else
        {
            trigger_error( 'Undefined property: ' . get_class($this) . '::$' . $name ) ;
        }
    }

    /**
     * @private
     */
    public function __set( $name , $value )
    {
        $method = NULL ;
        if( $name != '' )
        {
            $method = 'set' . ucfirst($name) ;
        }

        if( method_exists( $this, $method ) )
        {
            return $this->{ $method }( $value );
        }
        else
        {
            trigger_error( 'Undefined property: ' . get_class($this) . '::$' . $name ) ;
        }
    }

    /**
     * @private
     */
    private $_value = 0;

    /**
     * Emulates the >>> binary operator.
     */
    private function RRR( $a , $b )
    {
        return (int)( (float) $a / pow( 2 , (int) $b ) );
    }
}
