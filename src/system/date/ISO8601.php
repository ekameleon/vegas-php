<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace system\date;
use DateTime;
use DateTimeZone;

/**
 * The iso08601 tool class.
 */
class ISO8601
{
    /**
     * Creates a new ISO8601 instance.
     */
    public function __construct( $init = null )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The timezone used in the getTimeNow() method.
     */
    public $nowTimezone = "Europe/Paris" ;

    /**
     * The time expression to format the current time
     */
    public $nowTimeExpression = "Y-m-d H:i:s" ;

    /**
     * Returns the string expression of the current time 'NOW'.
     * @return the string expression of the current time 'NOW'.
     */
    public function getTimeNow()
    {
        $now = new DateTime( "NOW" );
        $now->setTimezone( new DateTimeZone( $this->nowTimezone ) );
        return $now->format( $nowTimeExpression );
    }

    /**
     * Returns the string expression of the current time 'NOW' with the iso8601 format.
     * @return the string expression of the current time 'NOW' with the iso8601 format.
     */
    public function getTimeNowISO8601( $GMT = FALSE )
    {
        $now = new DateTime( "NOW" );

        if( $GMT )
        {
            $now->setTimezone( new DateTimeZone( "Etc/Zulu" ) );
            return $now->format( "Y-m-d\TH:i:s\Z" );
        }
        else
        {
            $now->setTimezone( new DateTimeZone( $this->nowTimezone ) );
            return $now->format( DateTime::ISO8601 );
        }
    }

    /**
     * Formats the passed-in time expression in a iso8601 string representation.
     */
    public function formatTimeToISO8601( $timestr, $GMT = FALSE )
    {
        $time = new DateTime( $timestr );

        if( $GMT )
        {
            $time->setTimezone( new DateTimeZone( "Etc/Zulu" ) );
            return $time->format( "Y-m-d\TH:i:s\Z" );
        }
        else
        {
            $time->setTimezone( new DateTimeZone( $this->nowTimezone ) );
            return $time->format( DateTime::ISO8601 );
        }
    }
}

?>