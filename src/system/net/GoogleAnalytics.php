<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace system\net;

class GoogleAnalytics
{
    public function __construct( $trackingID, $hostname = "", $useSSL = FALSE, $useCacheBuster = FALSE )
    {
        $this->trackingID     = $trackingID;
        $this->hostname       = $hostname;
        $this->clientID       = $this->getCliendID();
        $this->useSSL         = $useSSL;
        $this->useCacheBuster = $useCacheBuster;
    }

    public $trackingID;
    public $hostname;
    public $clientID;

    public $useSSL;
    public $useCacheBuster;

    public function generateUUID()
    {
        return sprintf
        (
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function getCliendID()
    {
        if( isset($_COOKIE['_ga']) )
        {
            list( $version, $domainDepth, $cid1, $cid2 ) = preg_split( '[\.]', $_COOKIE["_ga"], 4 );

            $contents =
            [
                'version'     => $version,
                'domainDepth' => $domainDepth,
                'cid'         => $cid1 . '.' . $cid2
            ];

            $cid = $contents['cid'];
        }
        else if( isset( $_SERVER["HTTP_X_UUID"] ) )
        {
            $cid = $_SERVER["HTTP_X_UUID"];
        }
        else
        {
            $cid = $this->generateUUID();
        }

        return $cid;
    }

    public function socketPost( $url, $post_string, $ua = null )
    {
        $parts = parse_url( $url );

        // workout port and open socket
        $port = isset( $parts["port"] ) ? $parts["port"] : 80;
        $host = $parts[ "host" ];
        $path = $parts[ "path" ];

        $fp = fsockopen( $host, $port, $errno, $errstr, 30 );
        $success = $fp;

        if( $fp )
        {
            // create output string
            $output = "POST $path HTTP/1.1\r\n";

            if( is_string($ua) )
            {
                $output .= "User-Agent: $ua\r\n";
            }

            $post_len = strlen( $post_string );

            $output .= "Host: $host\r\n";
            $output .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $output .= "Content-Length: $post_len\r\n";
            $output .= "Connection: Close\r\n\r\n";
            $output .= isset($post_string) ? $post_string : "";

            // send output to $url handle
            $success = fwrite( $fp, $output );
            fclose( $fp );
        }
        else
        {
            echo "Error [$errno]: $errstr" . PHP_EOL;
        }

        return $success ? TRUE : FALSE;
    }

    public function sendHit( $data = NULL )
    {
        if( $data )
        {
            $url = "" ;

            if( $this->useSSL )
            {
                $url .= "https://ssl.google-analytics.com/collect";
            }
            else
            {
                $url .= "http://www.google-analytics.com/collect";
            }

            if( !empty($this->hostname) )
            {
                $data["dh"] = $this->hostname;
            }

            if( isset($_SERVER["REMOTE_ADDR"]) )
            {
                $data["uip"] = $_SERVER["REMOTE_ADDR"];
            }

            if( $this->useCacheBuster && isset($_SERVER["REQUEST_TIME"]) )
            {
                $data["z"] = $_SERVER["REQUEST_TIME"];
            }

            $query  = "?payload_data&";
            $query .= http_build_query( $data );

            return $this->socketPost
            (
                $url,
                $query,
                isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : 'none'
            );
        }

        return FALSE;
    }

    public function pageview( $path, $title = NULL )
    {
        $v   = 1;
        $tid = $this->trackingID;
        $cid = $this->clientID;

        $data =
        [
            "v"   => $v,
            "tid" => $tid,
            "cid" => $cid,
            "t"   => "pageview"
        ];

        if( !empty($title) )
        {
            $data[ "dt" ] = $title;
        }

        $data[ "dp" ] = $path;

        return $this->sendHit( $data );
    }

    public function exception( $message, $fatal = FALSE )
    {
        $v   = 1;
        $tid = $this->trackingID;
        $cid = $this->clientID;

        $data =
        [
            "v"   => $v,
            "tid" => $tid,
            "cid" => $cid,
            "t"   => "exception"
        ];

        $data[ "exd" ] = $message;
        $data[ "exf" ] = $fatal ? 1: 0;

        return $this->sendHit( $data );
    }
}

?>