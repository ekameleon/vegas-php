<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace system\logging ;

use Psr\Log\LoggerInterface ;
use Psr\Log\LogLevel ;

/**
 * A basic logger implementation.
 */
class Logger implements LoggerInterface
{
    /**
     * Creates a new Logger instance.
     * @param string $directory File path to the logging directory
     * @param integer $level One of the pre-defined level constants
     * @param string uuid The uuid of the application (optional).
     * @return void
     */
    public function __construct( $directory , $level = 7 )
    {
        $this->_directory = rtrim( $directory , '\\/' ) ;

        if ( $level === self::OFF )
        {
            return ;
        }

        $this->_path = $this->_directory . DIRECTORY_SEPARATOR . 'log_' . date('Y-m-d') . '.log' ;

        $this->_severityThreshold = $level ;

        if ( !file_exists( $this->_directory ) )
        {
            mkdir( $this->_directory, self::$_defaultPermissions, true ) ;
        }

        if ( file_exists( $this->_path ) && !is_writable( $this->_path ) )
        {
            $this->_status   = self::STATUS_OPEN_FAILED ;
            $this->_buffer[] = $this->_messages['writefail'] ;
            return ;
        }

        if ( ( $this->_file = fopen( $this->_path , 'a' ) ) )
        {
            $this->_status   = self::STATUS_LOG_OPEN ;
            $this->_buffer[] = $this->_messages['opensuccess'] ;
        }
        else
        {
            $this->_status   = self::STATUS_OPEN_FAILED ;
            $this->_buffer[] = $this->_messages['openfail'] ;
        }
    }

    /**
     * Destruct the instance.
     * @return void
     */
    public function __destruct()
    {
        if ( $this->_file )
        {
            fclose( $this->_file ) ;
        }
    }

    ///////////////////////////

    /**
     * Error severity, from low to high. From BSD syslog RFC, secion 4.1.1
     * @link http://www.faqs.org/rfcs/rfc3164.html
     */
    const EMERGENCY  = 0 ;  // Emergency: system is unusable
    const ALERT      = 1 ;  // Alert: action must be taken immediately
    const CRITICAL   = 2 ;  // Critical: critical conditions
    const ERROR      = 3 ;  // Error: error conditions
    const WARNING    = 4 ;  // Warning: warning conditions
    const NOTICE     = 5 ;  // Notice: normal but significant condition
    const INFO       = 6 ;  // Informational: informational messages
    const DEBUG      = 7 ;  // Debug: debug messages
    const OFF        = 8 ;  // Log nothing at all

    /**
     * Internal status codes
     */
    const STATUS_LOG_OPEN    = 1;
    const STATUS_OPEN_FAILED = 2;
    const STATUS_LOG_CLOSED  = 3;

    /**
     * @var array
     */
    const LEVELS =
    [
        LogLevel::EMERGENCY => self::EMERGENCY,
        LogLevel::ALERT     => self::ALERT,
        LogLevel::CRITICAL  => self::CRITICAL,
        LogLevel::ERROR     => self::ERROR,
        LogLevel::WARNING   => self::WARNING,
        LogLevel::NOTICE    => self::NOTICE,
        LogLevel::INFO      => self::INFO,
        LogLevel::DEBUG     => self::DEBUG
    ];

    ///////////////////////////

    /**
     * Writes a $message to the log with a severity level of ALERT.
     * @param string $message Information to log
     * @return void
     */
    public function alert( $message , array $context = [] )
    {
        $this->log( LogLevel::ALERT , $message , $context ) ;
    }

    /**
     * Writes a message to the log with a severity level of DEBUG.
     * @param string $message Information to log
     * @return void
     */
    public function debug( $message, array $context = [] )
    {
        $this->log( LogLevel::DEBUG , $message , $context ) ;
    }

    /**
     * Writes a $message to the log with a severity level of CRIT.
     * @param string $message Information to log
     * @return void
     */
    public function critical( $message , array $context = [] )
    {
        $this->log( LogLevel::CRITICAL , $message , $context ) ;
    }

    /**
     * Writes a $message to the log with a severity level of EMERG.
     * @param string $message Information to log
     * @return void
     */
    public function emergency( $message , array $context = [] )
    {
        $this->log( LogLevel::EMERGENCY , $message , $context ) ;
    }

    /**
     * Writes a $message to the log with a severity level of ERR. Most likely used with E_RECOVERABLE_ERROR.
     * @param string $message Information to log
     * @return void
     */
    public function error( $message , array $context = [] )
    {
        $this->log( LogLevel::ERROR , $message , $context );
    }

    /**
     * Writes a $message to the log with a severity level of INFO.
     * Any information can be used here, or it could be used with E_STRICT errors.
     * @param string $message Information to log
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->log( LogLevel::INFO , $message, $context ) ;
    }

    /**
     * Writes a message to the log with the given severity.
     * @param string $message Text to add to the log
     * @param integer $level Severity level of log message (use constants)
     * @return void
     */
    public function log( $level , $message , array $context = [] )
    {
        $num = array_key_exists( $level , self::LEVELS ) ? self::LEVELS[$level] : self::OFF ;

        if ( $this->_severityThreshold >= $num )
        {
            // ------- status

            $status = date( self::$_dateFormat ) . ' ' ;

            if( array_key_exists( $level , self::LEVELS ) )
            {
                $status .= strtoupper( $level ) ;
            }

            // ------- message

            $message = $status . ' ' . $message ;

            if( is_array($context) && count($context) > 0 )
            {
                $replace = [] ;
                foreach ($context as $key => $value)
                {
                    if (!is_array($value) && (!is_object($value) || method_exists($value, '__toString')))
                    {
                        $replace['{' . $key . '}'] = $value ;
                    }
                }
                $message = strtr($message, $replace);
            }

            // ------- write

            $this->writeFreeFormLine( $message . PHP_EOL ) ;
        }
    }

    /**
     * Writes a $message to the log with a severity level of NOTICE.
     * Generally corresponds to E_STRICT, E_NOTICE, or E_USER_NOTICE errors
     * @param string $message Information to log
     * @return void
     */
    public function notice( $message , array $context = [] )
    {
        $this->log( LogLevel::NOTICE , $message , $context );
    }

    /**
     * Writes a $message to the log with a severity level of WARN. Generally
     * corresponds to E_WARNING, E_USER_WARNING, E_CORE_WARNING, or E_COMPILE_WARNING
     * @param string $message Information to log
     * @return void
     */
    public function warn( $message , array $context = [] )
    {
        $this->log( LogLevel::WARNING , $message , $context ) ;
    }

    /**
     * Writes a $message to the log with a severity level of WARN. Generally
     * corresponds to E_WARNING, E_USER_WARNING, E_CORE_WARNING, or E_COMPILE_WARNING
     * @param string $message Information to log
     * @return void
     */
    public function warning( $message , array $context = [] )
    {
        $this->log( LogLevel::WARNING , $message , $context ) ;
    }

    /////////////////////////// global error callback methods

    /**
     * Invoked when a global error is sending.
     */
    public function onError( $code , $message , $file , $line )
    {
        $this->error( sprintf('[%s] - L:%s - C:%s - %s', $file, $line, $code, $message) ) ;
        return true ;
    }

    /**
     * Invoked when a global exception is sending.
     */
    public function onException( $exception )
    {
        $this->critical( '[' . get_class($exception) . '] ' . $exception->getMessage() ) ;
        return true ;
    }

    ///////////////////////////

    /**
     * Clear all logs in the log path.
     */
    public function clear()
    {
        $logs = scandir( $this->_directory ) ;
        foreach( $logs as $log )
        {
            if ( $log == '.' || $log == '..' )
            {
                continue ;
            }

            unlink( $this->_directory . '/' . $log ) ;
        }
    }

    /**
     * Returns (and removes) the last message from the queue buffer.
     * @return string
     */
    public function getMessage()
    {
        return array_pop( $this->_buffer ) ;
    }

    /**
     * Returns the entire message queue (leaving it intact)
     * @return array
     */
    public function getMessages()
    {
        return $this->_buffer ;
    }

    /**
     * Returns the directory of the logs files.
     * @return string
     */
    public function getDirectory()
    {
        return $this->_directory ;
    }

    /**
     * Returns the path of the logs files.
     * @return string
     */
    public function getPath()
    {
        return $this->_path ;
    }

    /**
     * Indicates the status of the logger instance.
     */
    public function getStatus()
    {
        return $this->_status ;
    }

    /**
     * Returns a String representation of the object.
     * @return string
     */
    public function __toString()
    {
        return "[" . get_class($this) . "]" ;
    }

    /**
     * Writes a line to the log without prepending a status or timestamp.
     * @param string $line Line to write to the log
     * @return void
     */
    public function writeFreeFormLine( $line )
    {
        if ( $this->_status == self::STATUS_LOG_OPEN && $this->_severityThreshold != self::OFF )
        {
            if ( fwrite( $this->_file , $line ) === false )
            {
                $this->_buffer[] = $this->_messages['writefail'] ;
            }
        }
    }

    ///////////////////////////

    /**
     * Holds messages generated by the class
     * @var array
     */
    private $_buffer = [];

    /**
     * Default severity of log messages, if not specified
     * @var integer
     */
    private static $_defaultSeverity = self::DEBUG ;

    /**
     * Valid PHP date() format string for log timestamps
     * @var string
     */
    private static $_dateFormat = 'Y-m-d G:i:s';

    /**
     * Octal notation for default permissions of the log file
     * @var integer
     */
    private static $_defaultPermissions = 0777 ;

    /**
     * The directory to the log file
     * @var string
     */
    private $_directory = "." ;

    /**
     * This holds the file handle for this instance's log file
     * @var resource
     */
    private $_file = null ;

    /**
     * Standard messages produced by the class. Can be modified for il8n
     * @var array
     */
    private $_messages = array
    (
        'writefail'   => 'The file could not be written to. Check that appropriate permissions have been set.',
        'opensuccess' => 'The log file was opened successfully.',
        'openfail'    => 'The file could not be opened. Check permissions.'
    );

    /**
     * Path to the log file
     * @var string
     */
    private $_path = null ;

    /**
     * Current minimum logging threshold
     * @var integer
     */
    private $_severityThreshold = self::DEBUG ;

    /**
     * Current status of the log file
     * @var integer
     */
    private $_status = self::STATUS_LOG_CLOSED ;
}

?>
