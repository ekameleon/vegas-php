<?php

/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

namespace system\signals;

/**
 * This class provides a fast Signaler implementation.
 * Example :
 * <pre>
 *  <?php
 *
 *  // ------------------------ imports
 *
 *  use system\signals\Signal;
 *  use system\signals\Receiver;
 *  use system\logging\Logger;
 *
 *  // ---------------------------------- constants
 *
 *  define ( '__APP__'  , __DIR__ . "/" ) ;
 *
 *  // ---------------------------------- errors
 *
 *  error_reporting( E_ALL & ~E_DEPRECATED );
 *
 *  // ---------------------------------- config
 *
 *  ini_set( 'display_errors'         , 'On' ) ;
 *  ini_set( 'display_startup_errors' , 'On' ) ;
 *
 *  // ---------------------------------- autoloader
 *
 *  require __APP__ . 'vendor/autoload.php';
 *
 *  // ---------------------------------- logger
 *
 *  $logger = new Logger( __APP__ . 'logs/' , 7 ) ;
 *
 *  set_error_handler     ( [ $logger , 'onError'     ] ) ;
 *  set_exception_handler ( [ $logger , 'onException' ] ) ;
 *
 *  // ----------------------------------
 *
 *  class Slot implements Receiver
 *  {
 *      public function __construct( Logger $logger )
 *      {
 *          $this->logger = $logger ;
 *      }
 *
 *      public $logger ;
 *
 *      public function receive()
 *      {
 *          $this->logger->debug('hello receiver') ;
 *      }
 *  }
 *
 *  // ----------------------------------
 *
 *  $slot1 = function() use ( $logger )
 *  {
 *      $logger->debug('hello slot') ;
 *  };
 *
 *  $slot2 = new Slot( $logger ) ;
 *
 *  // ----------------------------------
 *
 *  $signal = new Signal() ;
 *
 *  $signal->connect( $slot1 ) ; // use basic function
 *  $signal->connect( $slot2 ) ; // use a Receiver instance
 *
 *  $signal->emit() ;
 *
 * ?>
 * </pre>
 */
class Signal implements Signaler
{
    /**
     * Creates a new Signal instance.
     * @param array $receivers The Array collection of receiver objects to connect with this signal.
     */
    public function __construct( array $receivers = NULL )
    {
        $this->receivers = [] ;
        if ( isset( $receivers ) )
        {
            foreach( $receivers as $receiver )
            {
                $this->connect( $receiver ) ;
            }
        }
    }

    /**
     * @private
     */
    public function __get( $var )
    {
        $method = '_get' . ucfirst( strtolower( $var ) ) ;

        if( !is_callable($this, $method) )
        {
            throw new LogicException( __CLASS__ . "::$var is not readable.");
        }

        return $this->{ $method }() ;
    }

    /**
     * Indicates the number of receivers connected.
     * @return int the number of receivers connected.
     */
    public function getLength()
    {
        return count( $this->receivers ) ;
    }

    /**
     * Connects a Function or a Receiver object.
     * @param mixed $receiver The receiver to connect : a Function reference or a Receiver object.
     * @param int $priority Determinates the priority level of the receiver.
     * @param bool $autoDisconnect Apply a disconnect after the first trigger
     * @return bool TRUE If the receiver is connected with the signal emitter.
     */
    public function connect( $receiver , $priority = 0 , $autoDisconnect = FALSE )
    {
        if ( is_callable( $receiver ) || ( $receiver instanceof Receiver ) )
        {
            if ( $receiver instanceof Receiver )
            {
                $receiver = [ $receiver , 'receive' ] ;
            }

            if ( $this->hasReceiver( $receiver ) )
            {
                return FALSE ;
            }

            $this->receivers[] = new SignalEntry( $receiver , $priority , $autoDisconnect ) ;

            /// shell sort

            $n = count( $this->receivers ) ;
            if( $n > 0 )
            {
                $temp = NULL ;
                $i    = 0 ;
                $j    = 0 ;
                $inc = (int) ( ($n/2) + 0.5 ) ;
                while( $inc )
                {
                    for( $i = $inc ; $i<$n ; $i++)
                    {
                        $temp = $this->receivers[ $i ] ;
                        $j    = $i ;
                        while( ( $j >= $inc ) && ( $this->receivers[ (int) ($j - $inc) ]->priority < $temp->priority ) )
                        {
                            $this->receivers[$j] = $this->receivers[ (int) ($j - $inc) ] ;
                            $j = (int) ($j - $inc) ;
                        }
                        $this->receivers[$j] = $temp ;
                    }
                    $inc = (int) (($inc / 2.2) + 0.5) ;
                }
            }

            ///

            return TRUE ;
        }
        else
        {
            return FALSE ;
        }
    }

    /**
     * Returns true if one or more receivers are connected.
     * @return bool true if one or more receivers are connected.
     */
    public function connected()
    {
        return count( $this->receivers ) > 0 ;
    }

    /**
     * Disconnect the specified object or all objects if the parameter is NULL.
     * @param mixed $receiver The receiver to connect : a Function reference or a Receiver object.
     * @return bool TRUE if the specified receiver exist and can be unregister.
     */
    public function disconnect( $receiver = NULL )
    {
        if ( !isset( $receiver ) )
        {
            if ( $this->receivers.length > 0 )
            {
                $this->receivers = [] ;
                return TRUE ;
            }
            else
            {
                return FALSE ;
            }
        }

        if ( $receiver instanceof Receiver )
        {
            $receiver = array( $receiver , 'receive' ) ;
        }

        if ( is_callable( $receiver ) && count( $this->receivers ) > 0 )
        {
            foreach( $this->receivers as $key => $entry )
            {
                if( $entry->receiver === $receiver)
                {
                    unset( $this->receivers[$key] ) ;
                    return TRUE ;
                }
            }
        }
        return FALSE ;
    }

    /**
     * Emits the specified values to the receivers.
     * @param ...values All values to emit to the receivers.
     */
    public function emit()
    {
        if ( count( $this->receivers ) == 0 )
        {
            return ;
        }

        $args = func_get_args() ;

        foreach( $this->receivers as $key => $entry  )
        {
            if ( $entry->auto )
            {
                unset( $this->receivers[$key] ) ;
            }

            if( empty( $args ) )
            {
                call_user_func( $entry->receiver ) ;
            }
            else
            {
                call_user_func_array( $entry->receiver , $args ) ;
            }
        }
    }

    /**
     * Returns TRUE if the specified receiver is connected.
     * @param callable The receiver to search.
     * @return bool TRUE if the specified receiver is connected.
     */
    public function hasReceiver( $receiver )
    {
        if ( $receiver instanceof Receiver )
        {
            $receiver = array( $receiver , 'receive' ) ;
        }

        if ( is_callable( $receiver ) && count( $this->receivers ) > 0 )
        {
            foreach( $this->receivers as $key => $entry )
            {
                if( $entry->receiver === $receiver)
                {
                    return TRUE ;
                }
            }
        }
        return FALSE ;
    }

    /**
     * Returns the Array representation of all receivers connected with the signal.
     * @return the Array representation of all receivers connected with the signal.
     */
    public function toArray()
    {
        $ar = [] ;
        if ( count( $this->receivers ) > 0 )
        {
            foreach ( $this->receivers as $entry )
            {
                $ar[] = $entry->receiver ;
            }
        }
        return $ar ;
    }

    /**
     * Returns a String representation of the object.
     * @return a String representation of the object.
     */
    public function __toString()
    {
        return "[" . get_class( $this ) . ']' ;
    }

    /**
     * The Array representation of all receivers.
     * @param array
     */
    protected $receivers ;
}

?>