# VEGAS #

Vegas is an opensource framework. 

This repository contains the PHP version of the framework.

### Licences ###

Under tree licences : **MPL 1.1/GPL 2.0/LGPL 2.1**

 * Licence MPL 1.1 : http://www.mozilla.org/MPL/MPL-1.1.html
 * Licence GPL 2 : http://www.gnu.org/licenses/gpl-2.0.html
 * Licence LGPL 2.1 : http://www.gnu.org/licenses/lgpl-2.1.html

### About ###

 * Author : Marc ALCARAZ (aka eKameleon)
 * Mail : ekameleon@gmail.com
 * Link : http://www.ooopener.com

### Slack Community ###

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the VEGAS community on Slack !

### Editors ###

[Atom.io](https://atom.io/) or [PHP Storm](https://www.jetbrains.com/phpstorm/)

### History ###

 * 1998 : Flash 
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository 
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.